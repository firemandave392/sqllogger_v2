using Newtonsoft.Json;
using Oxide.Core.CSharp;
using Oxide.Core.Configuration;
using Oxide.Core.Database;
using Oxide.Core.Libraries.Covalence;
using Oxide.Core.MySql;
using Oxide.Core.Plugins;
using Oxide.Core;
using Rust;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System;
using UnityEngine.UI;
using UnityEngine;

namespace Oxide.Plugins 
{
	[Info("SQLLogger2", "FiremanDave", "2.0.01")]
	[Description("Redirects Rust logs to a specified MySQL database")]
	
	public class SQLLogger2 : RustPlugin 
	{
		//private Dictionary<uint, StorageType> itemTracker = new Dictionary<uint, StorageType>();
		//private Dictionary<BasePlayer, int32> loginTime = new Dictionary<BasePlayer, int>();
		readonly Core.MySql.Libraries.MySql _mySql = new Core.MySql.Libraries.MySql();
		private Connection _mySqlConnection = null;
		
		protected override void LoadDefaultConfig()
		{
			PrintWarning("Creating a new configuration file...");
			Config.Clear();
			Config["Host"] = "";
			Config["Database"] = "";
			Config["Port"] = 3306;
			Config["Username"] = "";
			Config["Password"] = "";
			Config["Version"] = "2.0.01";
			SaveConfig();
		}
	
	
	// Create and open MySQL connection to database server
	private void StartConnection() 
	{
		try {
			Puts("Opening connection to database server...");
			_mySqlConnection = _mySql.OpenDb(Config["Host"].ToString(), Convert.ToInt32(Config["Port"]), Config["Database"].ToString(), Config["Username"].ToString(), Config["Password"].ToString(), this);
			Puts("Connection to database server successful!");
		}
		catch (Exception failure)
		{
			Puts(failure.Message);
		}
	}
	
	// This is the command that will execute the query. Code reuse for the win.
	private void executeQuery(string query, params object[] data)
	{
		var sql = Sql.Builder.Append(query, data);
		_mySql.Insert(sql, _mySqlConnection);
	}
	
	// Create the tables if they do not exist.
	private void createTablesOnConnect() 
	{	
		try 
		{
			// Create player stats table <-- This needs to be revisited, since the original code inserting player stats was broken
			// executeQuery("CREATE TABLE IF NOT EXISTS player_stats (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, player_ip VARCHAR(128) NULL, player_state INT(1) NULL DEFAULT '0', player_online_time BIGINT(20) DEFAULT '0', player_last_login TIMESTAMP NULL, PRIMARY KEY (`player_id`), UNIQUE (`player_id`) ) ENGINE=InnoDB;");   
			
			// Create player resources gather table
			executeQuery("CREATE TABLE IF NOT EXISTS player_resource_gather (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, resource VARCHAR(255) NULL, amount INT(32), date TIMESTAMP NULL, location VARCHAR(255) NULL, activity VARCHAR(255) NULL, PRIMARY KEY (`id`), UNIQUE KEY `PlayerGather` (`player_id`,`resource`,`date`) ) ENGINE=InnoDB;");
			
			// Create player crafted items table
			executeQuery("CREATE TABLE IF NOT EXISTS player_crafted_item (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, item VARCHAR(128), amount INT(32), date TIMESTAMP NULL, PRIMARY KEY (`id`), UNIQUE KEY `PlayerItem` (`player_id`,`item`,`date`) ) ENGINE=InnoDB;");
			
			// Create player animal kills table
			executeQuery("CREATE TABLE IF NOT EXISTS player_kill_animal	(id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, animal VARCHAR(128), distance INT(11) NULL DEFAULT '0', weapon VARCHAR(128) NULL, time TIMESTAMP NULL, location VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
					
			// Create player kills table
			executeQuery("CREATE TABLE IF NOT EXISTS player_kill (id INT(11) NOT NULL AUTO_INCREMENT, killer_id BIGINT(20) NULL, killer_name VARCHAR(255) NULL, victim_id BIGINT(20) NULL, victim_name VARCHAR(255) NULL, bodypart VARCHAR(128), weapon VARCHAR(128), distance INT(11) NULL, time TIMESTAMP NULL, viclocation VARCHAR(255) NULL, killerlocation VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create player deaths table
			executeQuery("CREATE TABLE IF NOT EXISTS player_death (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, cause VARCHAR(128), count INT(11) NULL DEFAULT '1', date TIMESTAMP NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`), UNIQUE (`player_id`,`date`,`cause`) ) ENGINE=InnoDB;");
			
			// Create player building destruction table
			executeQuery("CREATE TABLE IF NOT EXISTS player_destroy_building(id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, building VARCHAR(128), building_grade VARCHAR(128), weapon VARCHAR(128), location VARCHAR(255) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create player built building tables
			executeQuery("CREATE TABLE IF NOT EXISTS player_place_building (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, building VARCHAR(128) NULL, grade VARCHAR(255) NULL, location VARCHAR(255) NULL DEFAULT '1', date TIMESTAMP NULL, PRIMARY KEY (`id`), UNIQUE (`player_id`,`date`,`building`) ) ENGINE=InnoDB;");
			
			// Create player deployables table
			executeQuery("CREATE TABLE IF NOT EXISTS player_place_deployable(id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, deployable VARCHAR(128) NULL, amount INT(32) NULL DEFAULT '1', date TIMESTAMP NULL, location VARCHAR(255) NULL, PRIMARY KEY (`id`), UNIQUE (`player_id`,`date`,`deployable`) ) ENGINE=InnoDB;");
			
			// Create TC auth list table
			executeQuery("CREATE TABLE IF NOT EXISTS player_authorize_list (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, cupboard VARCHAR(128) NULL, location VARCHAR(128) NULL, access VARCHAR(255) NULL DEFAULT '0', time VARCHAR(255) NULL, PRIMARY KEY (`id`), UNIQUE (`Cupboard`) ) ENGINE=InnoDB;");
			
			// Create player chat commands table
			executeQuery("CREATE TABLE IF NOT EXISTS player_chat_command (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, command VARCHAR(128) NULL, text VARCHAR(255) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create player connection log table
			executeQuery("CREATE TABLE IF NOT EXISTS player_connect_log	(id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, state VARCHAR(128) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create chat log table
			executeQuery("CREATE TABLE IF NOT EXISTS server_log_chat (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, chat_message VARCHAR(255), time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create server console log table
			executeQuery("CREATE TABLE IF NOT EXISTS server_log_console (id INT(11) NOT NULL AUTO_INCREMENT, server_message VARCHAR(255) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create admin log table
			executeQuery("CREATE TABLE IF NOT EXISTS admin_log	(id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(128) NULL, command VARCHAR(128) NULL, text VARCHAR(255) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create player hacks table
			executeQuery("CREATE TABLE IF NOT EXISTS player_hacks (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, location VARCHAR(128) NULL, activity VARCHAR(255) NULL, date TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create lock activity table
			executeQuery("CREATE TABLE IF NOT EXISTS lock_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, location VARCHAR(128) NULL, value VARCHAR(128) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create trap activity table
			executeQuery("CREATE TABLE IF NOT EXISTS trap_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, location VARCHAR(128) NULL, activity VARCHAR(255) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create door logging table
			executeQuery("CREATE TABLE IF NOT EXISTS door_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, location VARCHAR(128) NULL, activity VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create recycler table
			executeQuery("CREATE TABLE IF NOT EXISTS recycler_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, item_quantity INT(11) NULL, item_description VARCHAR(255) NULL, location VARCHAR(128) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create research table
			executeQuery("CREATE TABLE IF NOT EXISTS research_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, item_researched VARCHAR(255) NULL, research_location VARCHAR(255) NULL, time TIMESTAMP NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create vending machine table
			executeQuery("CREATE TABLE IF NOT EXISTS vending_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, item_purchased VARCHAR(255) NULL, purchase_price VARCHAR(255) NULL, location VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create mount table
			executeQuery("CREATE TABLE IF NOT EXISTS mount_activity (id INT(11) NOT NULL AUTO_INCREMENT,player_id BIGINT(20) NULL,  player_name VARCHAR(255) NULL, mount_description VARCHAR(255) NULL, activity VARCHAR(255) NULL, location VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create RP table
			executeQuery("CREATE TABLE IF NOT EXISTS rewards_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, activity VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create building upgrade and repair table
			executeQuery("CREATE TABLE IF NOT EXISTS upgrade_repair_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, item_type VARCHAR(255) NULL, grade VARCHAR(255) NULL, location VARCHAR(255) NULL, activity VARCHAR(255) NULL, date VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create sign activity table
			executeQuery("CREATE TABLE IF NOT EXISTS sign_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, sign_type VARCHAR(255) NULL, location VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create stash table
			executeQuery("CREATE TABLE IF NOT EXISTS stash_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, location VARCHAR(255) NULL, activity VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Loot added table
			executeQuery("CREATE TABLE IF NOT EXISTS loot_added (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, container_name VARCHAR(255) NULL, owner_id BIGINT(20) NULL,  chest_owner VARCHAR(255) NULL, location VARCHAR(255) NULL, date TIMESTAMP NULL, item VARCHAR(255) NULL, quantity VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Loot taken table
			executeQuery("CREATE TABLE IF NOT EXISTS loot_removed (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, container_name VARCHAR(255) NULL, owner_id BIGINT(20) NULL,  chest_owner VARCHAR(255) NULL, location VARCHAR(255) NULL, date TIMESTAMP NULL, item VARCHAR(255) NULL, quantity VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// Create turrets table
			executeQuery("CREATE TABLE IF NOT EXISTS turret_activity (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, turret_id VARCHAR(255) NULL, mode VARCHAR(255) NULL, location VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
			
			// BradleyAPC and Patrol Heli kills
			executeQuery("CREATE TABLE IF NOT EXISTS fun_kills (id INT(11) NOT NULL AUTO_INCREMENT, player_id BIGINT(20) NULL, player_name VARCHAR(255) NULL, vehicle VARCHAR(255) NULL, location VARCHAR(255) NULL, time VARCHAR(255) NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB;");
				
		}
		catch (Exception tablecreation) 
		{
			Puts(tablecreation.ToString());
		}
	}

	// Miscellaneous functions used later in the plugin.
	
	// Get date
	private string getDate()
	{
		return DateTime.Now.ToString("yyyy-MM-dd");
	}
	
	// Get date and time
	private string getDateTime()
	{
		return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
	}
	
	// Dictionary section
	
	// Human readable list of container names
	private string ContainerNameList(BaseEntity entity)
	{
		var entname = entity.ShortPrefabName;
		
		switch (entname)
		{
			case "bbq.deployed": return "Barbeque";
			case "box.wooden.large": return "Large Wood Box";
			case "bradley_crate": return "Bradley APC Crate";
			case "campfire": return "Campfire";
			case "ceilinglight.deployed": return "Ceiling Lamp";
			case "crate_basic": return "Basic Crate";
			case "crate_elite": return "Elite Crate";
			case "crate_mine": return "Mining Crate";
			case "crate_normal": return "Normal Crate";
			case "crate_normal_2": return "Normal Crate #2";
			case "crate_normal_2_food": return "Food Crate";
			case "crate_normal_2_medical": return "Medical Crate";
			case "crate_tools": return "Tool Crate";
			case "crudeoutput": return "Pumpjack Output";
			case "cupboard.tool.deployed": return "Tool Cupboard";
			case "dropbox.deployed": return "One Way Drop Box";
			case "fridge.deployed": return "Refrigerator";
			case "fuelstorage": return "Fuel Storage";
			case "furnace": return "Small Furnace";
			case "furnace.large": return "Large Furnace";
			case "heli_crate": return "Helicopter Crate";
			case "hopperoutput": return "Mining Quarry Output";
			case "lantern.deployed": return "Oil Lantern";
			case "locker.deployed": return "Locker";
			case "mailbox.deployed": return "Mailbox";
			case "refinery_small_deployed": return "Small Oil Refinery";
			case "repairbench_deployed": return "Repair Bench";
			case "researchtable_deployed": return "Research Table";
			case "searchlight.deployed": return "Search Light";
			case "skull_fire_pit": return "Halloween Skull fire pit";
			case "small_stash_deployed": return "Small Stash";
			case "stocking_large_deployed": return "Large Christmas Stocking";
			case "stocking_small_deployed": return "Small Christmas Stocking";
			case "survey_crater": return "Survey Charge results";
			case "survey_crater_oil": return "Survey Crater, Oil";
			case "tunalight.deployed": return "Tuna Can Lamp";
			case "wall.frame.shopfront.metal": return "Metal Shopfront";
			case "water_catcher_large": return "Large Water Catcher";
			case "water_catcher_small": return "Small Water Catcher";
			case "waterbarrel": return "Water Collection Barrel";
			case "waterstorage": return "Water Purifier";
			case "woodbox_deployed": return "Wood Storage Box";
			case "workbench1.deployed": return "Level One Work Bench";
			case "workbench2.deployed": return "Level Two Work Bench";
			case "workbench3.deployed": return "Level Three Work Bench";
			default: return null;
		}
	}
	
	// Human readable door list
	private string DoorPlacementList(Door door)
	{
		var entname = door.ShortPrefabName;
		
		switch(entname)
		{
			case "door.double.hinged.metal": return "Sheet Metal Double Door";
			case "door.double.hinged.toptier": return "Armored Double Door";
			case "door.double.hinged.wood": return "Wooden Double Door";
			case "door.hinged.metal": return "Sheet Metal Door";
			case "door.hinged.toptier": return "Armored Door";
			case "door.hinged.wood": return "Wooden Door";
			case "floor.ladder.hatch": return "Ladder Hatch";
			case "gates.external.high.stone": return "High External Stone Gate";
			case "gates.external.high.wood": return "High External Wood Gate";
			case "shutter.wood.a": return "Wooden Shutters";
			case "wall.frame.cell.gate": return "Prison Cell door";
			case "wall.frame.fence.gate": return "Chain Link Fence Gate";
			case "wall.frame.garagedoor": return "Garage Door";
			case "wall.frame.shopfront": return "Storefront";
			default: return null;
		}
	}
	
	// Human readable lock type
	private string CleanLock(BaseLock baselock)
	{
		var lockname = baselock.ShortPrefabName;
		
		switch (lockname)
		{
			case "lock.code": return "Code Lock";
			case "lock.key": return "Key Lock";
			default: return null;
		}
	}
	
	// Function for differentiating between placed and built items
	private string ObjectPlaced(BaseEntity entity)
	{
		var entityname = entity.ShortPrefabName;
		
		if (entityname.Contains("deployed"))
		{
			return "deployed";
		}
		return "built";
	}
	
	// Entity Formatting
	
	// Return three dimensional coordinates for specified BaseEntity in world (used primarily for kill/death tracking)
	private string EntityPosition(BaseEntity entity) => ($"at ({entity.transform.position.x} {entity.transform.position.y} {entity.transform.position.z})");
	
	// Return player's display nameof
	//private string PlayerName(BaseEntity player) => $"{player.displayName}({player.UserIdString})";
	
	// Standardize deploy append since Rust seems to use both .deployed and _deployed
	private string CleanUpEntity(BaseEntity entity) => entity.ShortPrefabName.Replace(".deployed", "").Replace("_deployed", "");
	
	// Return human readable ASCII container nameof
	private string ContainerName(BaseEntity entity) => ContainerNameList(entity) ?? CleanUpEntity(entity);
	
	// Get Item Name
	private string GetItemName(int itemId) => ItemManager.FindItemDefinition(itemId).displayName.english;
	
	// Returns a human readable door type
	private string DoorPlacement(Door door) => DoorPlacementList(door) ?? door.ShortPrefabName;

	// Get plugin and database version from configuration file
	private string GetConfigVersion(string value)
	{
		var curVersions = Convert.ToString(Config["Version"]);
		string[] version = curVersions.Split('.');
		var majorPluginUpdate = version[0];
		var minorPluginUpdate = version[1];
		var databaseupdate = version[2];
		if (value == "plugin")
		{
			return value = majorPluginUpdate+"."+minorPluginUpdate;
		}
		else if (value == "db")
		{
			return value = databaseupdate;
		}
		return value = majorPluginUpdate+"."+minorPluginUpdate;
	}
	
	private static BasePlayer GetPlayerFromContainer(ItemContainer container, Item item) => item.GetOwnerPlayer() ?? BasePlayer.activePlayerList.FirstOrDefault(p => p.inventory.loot.IsLooting() && p.inventory.loot.entitySource == container.entityOwner);
	
	
	// Need to revisit this code as well
	// private readonly Dictionary<ulong, ItemInfo> _item = new Dictionary<ulong, ItemInfo>();
	
	// private struct ItemInfo
	// {
		// public Item Item { get; set; }
	// }

	// private void ItemInfomation(Item item) => _item[item.uid] = new ItemInfo
	// {
		// Item = item,
	// };

	class StorageType
	{
		public string entityName;
		public string entityID;
		public string itemName;
		public int itemAmount;
		public string type;
	}
	
	// Plugin Command Section
	
	// Empty database, preserve structure
	
	[ConsoleCommand("logs.clear")]
	private void EmptyTablesCommand(ConsoleSystem.Arg arg)
	{
	    executeQuery("TRUNCATE player_resource_gather");
		executeQuery("TRUNCATE player_crafted_item");
		executeQuery("TRUNCATE player_bullets_fired");
		executeQuery("TRUNCATE player_kill_animal");
		executeQuery("TRUNCATE player_kill");
		executeQuery("TRUNCATE player_death");
		executeQuery("TRUNCATE player_destroy_building");
		executeQuery("TRUNCATE player_place_building");
		executeQuery("TRUNCATE player_place_deployable");
		executeQuery("TRUNCATE player_authorize_list");
		executeQuery("TRUNCATE player_chat_command");
		executeQuery("TRUNCATE player_connect_log");
		executeQuery("TRUNCATE server_log_chat");
		executeQuery("TRUNCATE server_log_console");
		executeQuery("TRUNCATE admin_log");
		executeQuery("TRUNCATE player_hacks");
		executeQuery("TRUNCATE lock_activity");
		executeQuery("TRUNCATE trap_activity");
		executeQuery("TRUNCATE door_activity");
		executeQuery("TRUNCATE recycler_activity");
		executeQuery("TRUNCATE research_activity");
		PrintWarning("Database tables have been cleared!");
	}
	
	// Completely drop the database and reload the plugin. Intended for database corruption.
	[ConsoleCommand("logs.drop.tables")]
	private void DropTablesCommand(ConsoleSystem.Arg arg)
	{
	    executeQuery("DROP TABLE player_resource_gather");
		executeQuery("DROP TABLE player_crafted_item");
		executeQuery("DROP TABLE player_bullets_fired");
		executeQuery("DROP TABLE player_kill_animal");
		executeQuery("DROP TABLE player_kill");
		executeQuery("DROP TABLE player_death");
		executeQuery("DROP TABLE player_destroy_building");
		executeQuery("DROP TABLE player_place_building");
		executeQuery("DROP TABLE player_place_deployable");
		executeQuery("DROP TABLE player_authorize_list");
		executeQuery("DROP TABLE player_chat_command");
		executeQuery("DROP TABLE player_connect_log");
		executeQuery("DROP TABLE server_log_chat");
		executeQuery("DROP TABLE server_log_console");
		executeQuery("DROP TABLE admin_log");
		executeQuery("DROP TABLE player_hacks");
		executeQuery("DROP TABLE lock_activity");
		executeQuery("DROP TABLE trap_activity");
		executeQuery("DROP TABLE door_activity");
		executeQuery("DROP TABLE recycler_activity");
		executeQuery("DROP TABLE research_activity");
		PrintWarning("Tables have been dropped. Plugin reloading.");
		try
		{
			rust.RunServerCommand("oxide.reload SQLLogger2");
			PrintWarning("SQLLogger2 plugin reloaded successfully");
		}
		catch (Exception reloadexception)
		{
			PrintWarning(reloadexception.Message);
		}
	}
	
	[ConsoleCommand("logs.reload")]
	private void ReloadCommand(ConsoleSystem.Arg arg)
	{
		try
		{
			PrintWarning("Reloading logger...");
			rust.RunServerCommand("oxide.reload SQLLogger2");
		}
		catch (Exception reloadexception)
		{
			PrintWarning(reloadexception.Message);
		}
	}
	
	// Other functions
	
	// Permissions check
	// bool hasPermission(BasePlayer player, string permissionName)
	// {
		// if (player.netconnection.authLevel > 1) return true;
		// return permission.UserHasPermission(player.userID.ToString(), permissionName);
	// }
	
	// Distance between combatants
	string GetDistances(BaseCombatEntity victim, BaseEntity attacker)
	{
		string distance = Convert.ToInt32(Vector3.Distance(victim.transform.position, attacker.transform.position)).ToString();
		return distance;
	}
	
	// Human readable animal names
	string GetFormattedAnimal(string animal)
	{
		string[] tokens = animal.Split('[');
		animal = tokens[0].ToUpper();
		return animal;
	}
	
	// Human readable body parts
	string formatBodyPartName(HitInfo hitInfo)
	{
		string bodypart = "Unknown";
		bodypart = StringPool.Get(Convert.ToUInt32(hitInfo?.HitBone)) ?? "Unknown";
		if ((bool)string.IsNullOrEmpty(bodypart)) bodypart = "Unknown";
		for (int i = 0; i < 10; i++)
		{
			bodypart = bodypart.Replace(i.ToString(), "");
		}
		bodypart = bodypart.Replace(".prefab", "");
		bodypart = bodypart.Replace("L", "");
		bodypart = bodypart.Replace("R", "");
		bodypart = bodypart.Replace("_", "");
		bodypart = bodypart.Replace(".", "");
		bodypart = bodypart.Replace("right", "");
		bodypart = bodypart.Replace("left", "");
		bodypart = bodypart.Replace("tranform", "");
		bodypart = bodypart.Replace("lowerjaweff", "jaw");
		bodypart = bodypart.Replace("rarmpolevector", "arm");
		bodypart = bodypart.Replace("connection", "");
		bodypart = bodypart.Replace("uppertight", "tight");
		bodypart = bodypart.Replace("fatjiggle", "");
		bodypart = bodypart.Replace("fatend", "");
		bodypart = bodypart.Replace("seff", "");
		bodypart = bodypart.Replace("Unknown", "Bleed to death");
		bodypart = bodypart.ToUpper();
		return bodypart;
	}

	// Remove non-printable characters from usernames
	static string EncodeNonAsciiCharacters(string value)
	{
		StringBuilder sb = new StringBuilder();
		foreach (char c in value)
		{
			if (c > 127)
			{
				// This value is beyond the ASCII limit
				string encodedValue = "";
				sb.Append(encodedValue);
			}
			else
			{
				sb.Append(c);
			}
		}
		return sb.ToString();
	}
	
	// Get plugin version and database version from config file
	private string getConfigVersion(string value) 
	{
		var curVersions = Convert.ToString(Config["Version"]);
		string[] version = curVersions.Split('.');
		var majorPluginUpdate = version[0];
		var minorPluginUpdate = version[1];           
		var databaseUpdate = version[2];
		if (value == "plugin") 
		{
			return value = majorPluginUpdate+"."+minorPluginUpdate;
		}
		else if  (value == "db")
		{
			return value = databaseUpdate;	
		}
		return value = majorPluginUpdate+"."+minorPluginUpdate;
		}	
	
	// Plugin initialization
	void Init()
	{
		// Ensure this is being loaded in Rust, rather than another game. This plugin is only for Rust.
		if !RUST
			throw new NotSupportException("This plugin is written for Rust only. It will not work with any other games.");
		endif
		
		// Version breakout. Must check to ensure that the database version running is the same as the version of the plugin.
		string currentVersion = Version.ToString();
		string[] version = currentVersion.Split('.');
		var majorPluginUpdate = version[0];
		var minorPluginUpdate = version[1];
		var databaseVersion = version[2];
		var pluginVersion = majorPluginUpdate+"."+minorPluginUpdate;
		Puts("Plugin Version: "+majorPluginUpdate+"."+minorPluginUpdate+". Database version: "+databaseVersion);
		
		// Logic to actually perform the check on the plugin version and update the config
		
		if (pluginVersion != getConfigVersion("plugin"))
		{
			Puts("New SQLLogger version "+pluginVersion+". Old version of SQLLogger is "+getConfigVersion("plugin") );
			Config["Version"] = pluginVersion+"."+databaseVersion;
			SaveConfig();
		}
		
		// This logic is for checking the database version against the plugin's database version. The DB version number will only change if there are changes to the DB structure.
		
		if (databaseVersion != getConfigVersion("db") )
		{
			Puts("Warning! The database version has changed! Old version is "+databaseVersion+". The new version is "+getConfigVersion("db")+".");
			PrintWarning("Database version has changed. Please drop the database and reload the plugin to update the database structure.");
			Config["Version"] = pluginVersion+"."+databaseVersion;
			SaveConfig();
		}
	}
	
	// Once the plugin loads, open the connection to the database server and create the tables if they do not exist.
	void Loaded()
	{
		StartConnection();
		createTablesOnConnect();
	}
	
	// Once the plugin is unloaded, close the databse connection to free system resources.
	void Unloaded()
	{
		timer.Once(5, () =>
		{
			_mySql.CloseDb(_mySqlConnection);
			_mySqlConnection = null;
		});
	}
	
	// Create player hooks. They are in no particular order. They'll be ordered based on type of action in a later revision.
	
	// PLAYER GATHERING
	
	// Log player gathering resource from an actual dispenser.
	void OnDispenserGather(ResourceDispenser dispenser, BaseEntity entity, Item item)
	{
		if(entity is BasePlayer)
		{
			// Encode characters to avoid crashes and SQL injection attacks
			string properName = EncodeNonAsciiCharacters(((BasePlayer)entity).displayName);
			// Insert the data into the database. <-- Temporarily disabled due to changes in Oxide.
			// executeQuery("INSERT INTO player_resource_gather (player_id, resource, amount, date, player_name, location, activity) VALUES (@0, @1, @2, @3, @4, @5, @6) ON DUPLICATE KEY UPDATE amount = amount +"item.amount, ((BasePlayer)entity).userId, item.info.displayName.english, item.amount, getDateTime(), properName, EntityPosition(entity), "Resource gathered" );
		}
	}

	// Player picks up resource from node
	void OnCollectiblePickup(Item item, BasePlayer player)
	{
		string properName = EncodeNonAsciiCharacters(((BasePlayer)player).displayName);
		executeQuery("INSERT INTO player_resource_gather (player_id, resource, amount, date, player_name, location, activity) VALUES (@0, @1, @2, @3, @4, @5, @6) ON DUPLICATE KEY UPDATE amount = amount +"+item.amount, ((BasePlayer)player).userID, item.info.displayName.english, item.amount, getDateTime(), properName, EntityPosition(player), "Resource picked up" );
	}
	
	// Player picks item up
	void OnItemAction(Item item, string action, BasePlayer player)
	{
		// var player = item.parent.playerOwner;
		if (action.ToLower() != "drop")
		{
			executeQuery("INSERT INTO player_resource_gather (player_id, resource, amount, date, player_name, location, activity) VALUES (@0, @1, @2, @3, @4, @5, @6) ON DUPLICATE KEY UPDATE amount = amount +"+item.amount, ((BasePlayer)player).userID, item.info.displayName.english, item.amount, getDateTime(), EncodeNonAsciiCharacters(((BasePlayer)player).displayName), EntityPosition(player), "Item picked up" );
		}
		else if (action.ToLower() == "drop")
		{
			executeQuery("INSERT INTO player_resource_gather (player_id, resource, amount, date, player_name, location, activity) VALUES (@0, @1, @2, @3, @4, @5, @6) ON DUPLICATE KEY UPDATE amount = amount +"+item.amount, ((BasePlayer)player).userID, item.info.displayName.english, item.amount, getDateTime(), EncodeNonAsciiCharacters(((BasePlayer)player).displayName), EntityPosition(player), "Item dropped" );
		}
	}

	// Player item crafting <-- Temporarily disabled due to changes in Oxide
	void OnItemCraftFinished(ItemCraftTask task, Item item)
	{
		// executeQuery("INSERT INTO player_crafted_item (player_id, player_name, item, amount, date) VALUES (@0, @1, @2, @3, @4) ON DUPLICATE KEY UPDATE amount = amount +"+item.amount, task.owner.userID, PlayerName(task.owner), item.info.displayName.english, item.amount, getDateTime() );
	}
	
	// BUILDING, UPGRADING AND REPAIRS
	
	// Placing items or building prefabs
	void OnEntityBuilt(Planner plan, GameObject go, HeldEntity heldentity, BuildingGrade.Enum grade)
	{
		string name = plan.GetOwnerPlayer().displayName;
		ulong playerID = plan.GetOwnerPlayer().userID;
		var placedObject = go.ToBaseEntity();
		if (placedObject is BuildingBlock)
		{
			string item_name = ((BuildingBlock)placedObject).blockDefinition.info.name.english;
			string item_grade = ((BuildingBlock)placedObject).currentGrade.gradeBase.name;
			executeQuery("INSERT INTO player_place_building (player_id, player_name, building, grade, location, date) VALUES (@0, @1, @2, @3, @4, @5)", playerID, name, placedObject, item_grade, EntityPosition(placedObject), getDateTime() );
		}
		else if (plan.isTypeDeployable)
		{
			string item_name = plan.GetOwnerItemDefinition().displayName.english;
			executeQuery("INSERT INTO player_place_deployable (player_id, player_name, deployable, date, location) VALUES (@0, @1, @2, @3, @4)", playerID, name, item_name, getDateTime(), EntityPosition(placedObject) );
		}
		if (plan.GetOwnerItemDefinition().shortname == "cupboard.tool")
		{
			var cupboard = go.GetComponent<BuildingPrivlidge>();
			BasePlayer player = plan.GetOwnerPlayer();
			OnCupboardAuthorize(cupboard, player);
			OnCupboardAuthorize(cupboard, player); // still using this dirty fix for setting access.
		}
	}
	
	// Building upgrades
	void OnStructureUpgrade(BaseCombatEntity entity, BasePlayer player, BuildingGrade.Enum grade)
	{
		executeQuery("INSERT INTO upgrade_repair_activity (player_name, item_type, grade, location, activity, date) VALUES (@0, @1, @2, @3, @4, @5)", ((BasePlayer) player).displayName.ToString(), entity.ShortPrefabName, grade, EntityPosition(entity), "Upgraded", getDateTime());
	}
	
	// Building repairs
	void OnStructureRepair(BaseCombatEntity entity, BasePlayer player, BuildingGrade.Enum grade)
	{
		executeQuery("INSERT INTO upgrade_repair_activity (player_name, item_type, grade, location, activity, date) VALUES (@0, @1, @2, @3, @4, @5)", ((BasePlayer) player).displayName.ToString(), entity.ShortPrefabName, grade, EntityPosition(entity), "Repaired", getDateTime());
	}
	
	// LOOTING LOGS
	
	// Item added to container
	private void OnItemAddedToContainer(ItemContainer container, Item item)
	{
		if (container == null || item == null) return;
		if (container.entityOwner == null) return;
		
		BaseEntity box = container.entityOwner;
		
		if (box == null) return;
		
		BasePlayer owner = BasePlayer.Find(box.OwnerID.ToString());
		BasePlayer player = GetPlayerFromContainer(container, item);
		
		if (!player) return;
		
		// ItemInformation(item);
		
		executeQuery("INSERT INTO loot_added (player_id, player_name, container_name, owner_id, chest_owner, location, date, item, quantity) VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8)", ((BasePlayer)player).userID, player.displayName, ContainerName(box), owner.userID, (owner.displayName ?? owner.UserIDString), EntityPosition(player), getDateTime(), GetItemName(item.info.itemid), item.amount);
		
	}
	
	// Item removed from container
	private void OnItemRemovedFromContainer(ItemContainer container, Item item)
	{
		// We don't care about logging this if the container or item is blank.
		if (container == null || item == null) return;
		// We don't care about logging it if there's no player owner for the container, either.
		if (container.entityOwner == null) return;
		
		BaseEntity box = container.entityOwner;
		
		if (box == null) return;
		
		BasePlayer owner = BasePlayer.Find(box.OwnerID.ToString());
		BasePlayer player = GetPlayerFromContainer(container, item);
		
		if (!player) return;
		
		// ItemInformation(item);
		
		executeQuery("INSERT INTO loot_removed (player_id, player_name, container_name, owner_id, chest_owner, location, date, item, quantity) VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8)", ((BasePlayer)player).userID, player.displayName, ContainerName(box), owner.userID, (owner.displayName ?? owner.UserIDString), EntityPosition(player), getDateTime(), GetItemName(item.info.itemid), item.amount);
	
	}
		
	// Player, building and NPC deaths
	
	private void OnEntityDeath(BaseCombatEntity victim, HitInfo info)
	{
		BasePlayer bplayer = null;
		IPlayer iplayer = null;
		
		// If the victim identity is empty or null, skip it.
		if (victim == null || String.IsNullOrWhiteSpace(victim.name)) return;
		
		// If the victim is server junk, a dead body, or plants, move on with our lives
		if ((victim.name.Contains("servergibs") || victim.name.Contains("corpse")) || victim.name.Contains("assets/prefabs/plants/")) return;
		
		// If the combat initiator is null or blank, move on.
		if (info == null || info.Initiator == null || String.IsNullOrWhiteSpace(info.Initiator.name)) return;
		
		// Try to capture the data
		try
		{
			bplayer = info.Initiator.ToPlayer();
			// If the base player and initiating player are not null, set variables
			if (bplayer != null && bplayer.IPlayer != null)
			{
				iplayer = bplayer.IPlayer;
			}
		}
		// No one cares if we can't get the data right now.
		catch {}
		
		// If the victim is the helicopter, print it to chat.
		if (victim is BaseHelicopter || victim.ShortPrefabName.Contains("patrolheli"));
		{
			PrintToChat("Patrol helicopter was killed by "+iplayer.Name);
		}
		
		string resource = null;
		string ptype = null;

		// Log Bradley, Patrol Heli and Chinook kills
		if (victim is BaseHelicopter || victim.name.Contains("patrolheli") || victim is CH47HelicopterAIController || victim.name.Contains("ch47") || victim is BradleyAPC || victim.name.Contains("bradleyapc"))
		{
		
			// Log BradleyAPC kills
			if (victim is BradleyAPC || victim.name.Contains("bradleyapc"))
			{
				executeQuery("INSERT INTO fun_kills (player_name, vehicle, location, time) VALUES (@0, @1, @2, @3)", iplayer.Name, "Bradley APC", EntityPosition(victim), getDateTime() );
			}
		
			// Log Patrol Helicopter kills
			else if (victim is BaseHelicopter || victim.name.Contains("patrolheli"))
			{
				executeQuery("INSERT INTO fun_kills (player_name, vehicle, location, time) VALUES (@0, @1, @2, @3)", iplayer.Name, "Patrol Helicopter", EntityPosition(victim), getDateTime() );
			}
			
			// Log Chinook helicopter kills
			else if (victim is CH47HelicopterAIController || victim.name.Contains("ch47"))
			{
				executeQuery("INSERT INTO fun_kills (player_name, vehicle, location, time) VALUES (@0, @1, @2, @3)", iplayer.Name, "CH47 Chinook", EntityPosition(victim), getDateTime() );
			}
			
			// If there's no initiating player or base player, skip it.
			if (iplayer == null || bplayer == null)
			{
				return;
			}
		}
		
		// Log player kills, skip NPC kills
		BasePlayer victimplayer = null as BasePlayer;
		
		if (victim.name.Contains("assets/rust.ai/agents/npcplayer") || victim.name.Contains("scientist") || victim.name.Contains("human"))
		{
			return;
		}
		
		// Log actual player kills
		if (victim is BasePlayer)
		{
			bool isFriend = false;
			victimplayer = victim.ToPlayer();
			
			// If the victim is empty, don't bother with it.
			if (victimplayer == null || victimplayer.userID == null | String.IsNullOrWhiteSpace(bplayer.UserIDString) || String.IsNullOrWhiteSpace(victimplayer.UserIDString)) return;
			
			// No one cares about bot kills, fuck 'em. This catches sneaky NPCs
			else if (String.Compare(victimplayer.userID.ToString(), "75000000000000000") != 1) return;
			
			// This catches actual player deaths, logging suicides and murders
			else if (String.Compare(iplayer.Id, victimplayer.userID.ToString()) == 0 || String.Compare(bplayer.UserIDString,victimplayer.UserIDString) == 0)
			{
				// Suicide logging
				executeQuery("INSERT INTO player_kill (killer_id, killer_name, victim_id, victim_name, bodypart, weapon, distance, time) VALUES (@0, @1, @2, @3, @4, @5, @6, @7, @8, @9)", iplayer.Id, iplayer.Name, victimplayer.userID.ToString(), victimplayer.displayName.ToString(), formatBodyPartName(info), "Suicide", "0", getDateTime(), EntityPosition(victimplayer), "" );
			}
			else
			{
				// Murder logging
				executeQuery("INSERT INTO player_kill (killer_id, killer_name, victim_id, victim_name, bodypart, weapon, distance, time) VALUES (@0, @1, @2, @3, @4, @5, @6, @7)", iplayer.Id, iplayer.Name, victimplayer.userID.ToString(), victimplayer.displayName.ToString(), formatBodyPartName(info), ((BasePlayer)victimplayer.lastAttacker).GetActiveItem().info.displayName.english, victimplayer.Distance2D(info?.Initiator?.ToPlayer()), getDateTime(), EntityPosition(victimplayer), EntityPosition(bplayer) );
			}
		}
		
		// Log building destruction
		if (victim is BuildingBlock)
		{
			var block = victim as BuildingBlock;
			var attacker = info.InitiatorPlayer;
			var weapon = attacker.GetHeldEntity();
			// Skip null data
			if (victim == null || info?.InitiatorPlayer == null) return;
			
			// If the held weapon is null, fuck it.
			if (weapon == null)	return;
			
			// If the attacked block is empty, no one gives a shit.
			if (block == null) return;
			
			// Execute the query
			executeQuery("INSERT INTO player_destroy_building (player_id, player_name, building, building_grade, weapon, location, time) VALUES (@0, @1, @2, @3, @4, @5, @6)", attacker.UserIDString, attacker.displayName,block.ShortPrefabName, ((BuildingBlock)block).currentGrade.gradeBase.name.ToUpper(), weapon, EntityPosition((BuildingBlock)block), getDateTime() );
		}
	}
	
	// Logging tool cupboard actions
	
	// Log cupboard authorization
	void OnCupboardAuthorize(BuildingPrivlidge privilege, BasePlayer player)
	{
		var priv = privilege.ToString();
		var pid = player.userID.ToString();
		var playername = player.displayName.ToString();
		var whereareyou = EntityPosition(player);
		executeQuery("INSERT INTO player_authorize_list (player_id, player_name, cupboard, location, access, time) VALUES (@0, @1, @2, @3, @4, @5) ON DUPLICATE KEY UPDATE access = 'Authorized'", pid, playername, priv, " ", whereareyou, "Authorized", getDateTime());
	}
	
	// Log cupboard deauthorize
	void OnCupboardDeauthorize(BuildingPrivlidge privilege, BasePlayer player)
	{
		var priv = privilege.ToString();
		var pid = player.userID.ToString();
		var playername = player.displayName.ToString();
		var whereareyou = EntityPosition(player);
		executeQuery("INSERT INTO player_authorize_list (player_id, player_name, cupboard, location, access, time) VALUES (@0, @1, @2, @3, @4, @5) ON DUPLICATE KEY UPDATE access = 'Deauthorized'", pid, playername, priv, "", whereareyou, getDateTime());
	}
	
	// Log clearing cupboard auth list
	void OnCupboardClearList(BuildingPrivlidge privilege, BasePlayer player)
	{
		var priv = privilege.ToString();
		var pid = player.userID.ToString();
		var playername = player.displayName.ToString();
		var whereareyou = EntityPosition(player);
		executeQuery("INSERT INTO player_authorize_list (player_id, player_name, cupboard, location, access, time) VALUES (@0, @1, @2, @3, @4, @5) ON DUPLICATE KEY UPDATE access = 0", pid, playername, priv, "", whereareyou, getDateTime()," WHERE cupboard=",pid);
	}

	}
// End of plugin file
}	
