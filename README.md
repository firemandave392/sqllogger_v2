# Rust SQL Logger plugin

## A plugin for redirecting rust logs from text files to a MySQL database

At this time, the code compiles and works. However, it is throwing some errors I still need to chase down.

### Known Issues
See list here: https://gitlab.com/firemandave392/sqllogger_v2/-/issues
